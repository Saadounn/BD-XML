/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdxmlnosql;

import com.mongodb.BasicDBObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author Banana
 */
public class TraitDocument {
   
    public String JsonToString(String path) throws IOException{
        String res=null;
        BufferedReader input = new BufferedReader(new FileReader(path));
        while(input.readLine()!=null)
    {
       res+=input.readLine();
    }
        
    return res;
    }
    
   HashMap<String, String> Getmap(String str) {
    String[] tokens = str.split("&");
    HashMap<String, String> map = new HashMap<String, String>();
    for(int i=0;i<tokens.length;i++)
    {
        String[] strings = tokens[i].split("=");
        if(strings.length==2)
         map.put(strings[0], strings[1].replaceAll("%2C", ","));
    }

    return map;
    }
    
    
    
}
