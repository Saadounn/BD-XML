/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdxmlnosql;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Map;
import org.json.simple.JSONObject;

/**
 *
 * @author Banana
 */
public class ExportationFiles {
    DBCollection collection;
    ArrayList<String> pmids;
    public ExportationFiles() throws UnknownHostException{
        collection = new JavaMongoDBConnection().getCollection();
        pmids = new ArrayList<String>();
        
    }
     public void getPMID(){
	BasicDBObject requete = new BasicDBObject();
	BasicDBObject champs = new BasicDBObject();
        //l'existance d'un PMID dans medlinecitation de pubmedArticle
	champs.put("PubmedArticle.MedlineCitation.PMID", 1);
	DBCursor cursor = collection.find(requete, champs);
        System.out.println("Avant while");
	while(cursor.hasNext()) {
            JSONObject allFields=new JSONObject(cursor.next().toMap());
            JSONObject pubmed = new JSONObject((Map)allFields.get("PubmedArticle"));
            JSONObject medlinecitation = new JSONObject((Map)pubmed.get("MedlineCitation"));
            JSONObject pmid = new JSONObject((Map)medlinecitation.get("PMID"));
            pmids.add(new String(pmid.get("__text").toString()));
            System.out.println("-----------------------------------------");
            System.out.println(new String(pmid.get("__text").toString()));
            
            
            System.out.println("-----------------------------------------");
	}
        System.out.println("Apres while");
    }
    
    
    // recupere tous les pmids pour avoir les meshheading et les abstractText de chaque pmid
    public ArrayList<String> getListPMIDs(){
	BasicDBObject requete = new BasicDBObject();
	BasicDBObject champs = new BasicDBObject();
        //l'existance d'un PMID dans medlinecitation de pubmedArticle
	champs.put("PubmedArticle.MedlineCitation.PMID", 1);
	DBCursor cursor = collection.find(requete, champs);
	while(cursor.hasNext()) {
            JSONObject allFields=new JSONObject(cursor.next().toMap());
            JSONObject pubmed = new JSONObject((Map)allFields.get("PubmedArticle"));
            JSONObject medlinecitation = new JSONObject((Map)pubmed.get("MedlineCitation"));
            JSONObject pmid = new JSONObject((Map)medlinecitation.get("PMID"));
            System.out.println("00000001111111111"+pmid.toString());
            System.out.println("Le contenu de pmid : "+new String(pmid.get("__text").toString()));
            pmids.add(new String(pmid.get("__text").toString()));
	}
    return pmids;
    }
    // recuperer les meshheading d'un PubmedArticle ayant comme pmid "pmid" passé en paramètre
    public ArrayList<String> getListMeshheadings(String PMID){
        ArrayList<String> liste = new ArrayList<String>();
	BasicDBObject where = new BasicDBObject("PubmedArticle.MedlineCitation.PMID.__text", PMID);
	DBCursor cursor = collection.find(where);
	while(cursor.hasNext()) {
            //pour recuperer un json object ayant le pubmedArticle ciblé
            JSONObject allFields=new JSONObject(cursor.next().toMap());
            System.out.println("allfields"+allFields.toString());
            // pour récupérer le contenue de PubmedArticle ciblé (surement contient medlinecitation)
            JSONObject pubmed = new JSONObject((Map)allFields.get("PubmedArticle"));
            //pour récuperer le contenue du medlinecitation de pubmedarticle
            JSONObject medlinecitation = new JSONObject((Map)pubmed.get("MedlineCitation"));
            //pour récupere la MeshHeadingList contenant les différents meshheadings
            JSONObject meshHeadingList = new JSONObject((Map)medlinecitation.get("MeshHeadingList"));
            //on doit tester le contenu de meshheading car on peut avoir un meshheading ou bien une liste de meshheading
       
            Object meshheading = meshHeadingList.get("MeshHeading");
            // si la liste de meshheading contient un seul meshheading
            if(meshheading instanceof BasicDBObject){
                JSONObject meshObject = new JSONObject(((BasicDBObject) meshheading).toMap());
                JSONObject descriptorname = new JSONObject((Map) meshObject.get("DescriptorName"));
		liste.add(new String(descriptorname.get("__text").toString()));
                System.out.println(new String(descriptorname.get("__text").toString()));
            }
            else{
		BasicDBList listeMeshHeading = (BasicDBList) meshHeadingList.get("MeshHeading");
		for(Object mesh : listeMeshHeading){
                    Map map = (Map)mesh;
                    JSONObject meshObject = new JSONObject(map);
                    JSONObject descriptorname = new JSONObject((Map) meshObject.get("DescriptorName"));
                    liste.add(new String(descriptorname.get("__text").toString()));
                    System.out.println(new String(descriptorname.get("__text").toString()));
                }
            }
	}
	return liste;	
    }
    
    //Abstracttexts
    
    public ArrayList<String> getListAbstracttext(String PMID){
        ArrayList<String> liste = new ArrayList<String>();
        BasicDBObject where = new BasicDBObject("PubmedArticle.MedlineCitation.PMID", PMID);
	DBCursor cursor = collection.find(where);
	while(cursor.hasNext()) {
            //pour recuperer un json object ayant le pubmedArticle ciblé
            JSONObject object=new JSONObject(cursor.next().toMap());
            // pour récupérer le contenue de PubmedArticle ciblé (surement contient medlinecitation)
            JSONObject PubmedArticle = new JSONObject((Map)object.get("PubmedArticle"));
            //pour récuperer le contenue du medlinecitation de pubmedarticle
            JSONObject medlinecitation = new JSONObject((Map)PubmedArticle.get("MedlineCitation"));
            JSONObject article = new JSONObject((Map)medlinecitation.get("Article"));
             //pour récuperer AbstractList contenant les différents abstracttext
            JSONObject abstractList = new JSONObject((Map)article.get("Abstract"));
            Object abstractt = abstractList.get("AbstractText");
            if(abstractt instanceof BasicDBObject){
                JSONObject abObject = new JSONObject(((BasicDBObject) abstractt).toMap());
		liste.add(new String(abObject.get("AbstractText").toString()));
            }
            else{
		BasicDBList listeab = (BasicDBList) abstractList.get("AbstractText");
		for(Object ab : listeab){
                    Map map = (Map)ab;
                    JSONObject abObject = new JSONObject(map);
                    liste.add(new String(abObject.get("AbstractText").toString()));
                }
            }
	}
	return liste;	
    }
    
}
