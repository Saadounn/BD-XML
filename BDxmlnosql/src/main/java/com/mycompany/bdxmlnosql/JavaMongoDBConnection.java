/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdxmlnosql;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import java.io.IOException;
import java.net.UnknownHostException;
/**
 *
 * @author Banana
 */
public class JavaMongoDBConnection {
    private DBCollection collection;
    
    public void setCollection(DBCollection collection){
        this.collection = collection;
    }
    
    public DBCollection getCollection(){
        return this.collection;
    }
    
    public JavaMongoDBConnection() throws UnknownHostException{
        // To connect to mongodb server
         MongoClient mongoClient = new MongoClient( "localhost" , 27017 );		
         // Now connect to your databases
         DB db = mongoClient.getDB( "test" );
//         DBCollection collection = db.getCollection("dummyColl");
         collection = db.getCollection("test");

         System.out.println("Connexion à la base de données réussie"); 
    }

//         
//         
//          TraitDocument TD=new TraitDocument();
//        String path="";
//        String res= TD.JsonToString(path);
//        BasicDBObject file=new BasicDBObject();
//        file.putAll(TD.Getmap(res));
//        collection.insert(file);

    }

