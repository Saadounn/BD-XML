/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdxmlnosql;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Banana
 */
public class Main {
   
    public static void main(String[] args) throws UnknownHostException, IOException, ParseException{

        /*--------- Importation ---------------*/
        // fichier 1
        ImportattionJSON importation = new ImportattionJSON("C:\\Users\\Banana\\Desktop\\files\\rare.json");
        //removeAll(importation.getCollection());
        //importation.Import();
       
       
       
//        
//       
        /*--------- Exportation---------------*/
        
        
        ExportationFiles exportation = new ExportationFiles();
        exportation.getPMID();
         ExportToRdf(exportation);
    
    
                //ExportationFiles EF=new ExportationFiles();
                //ArrayList<String> pmids;
               

		
	}
 public static void ExportToTxt(ExportationFiles EF) throws IOException{
                 ArrayList<String> pmids;
                 pmids=EF.getListPMIDs();
               
                ArrayList<String> meshheadings;
     
     		File newFile = new File("resultabstract.txt");
		if(newFile.exists()){
			newFile.delete();
		}
		newFile.createNewFile();
//		
		try{
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile.getAbsolutePath(), true));
			//pmids=EF.getListPMIDs();
			for(int i=0; i<pmids.size();i++){
				String PMID=pmids.get(i);
				//*********** Partie PMID *********************
				bufferedWriter.write("**************************************************");
				bufferedWriter.newLine();
				bufferedWriter.write("PMID : "+PMID);
				bufferedWriter.newLine();
				bufferedWriter.write("**************************************************");
				bufferedWriter.newLine();
				//********* Partie meshHeadings *****************
				bufferedWriter.write("Meshheadings : ");
				meshheadings=EF.getListMeshheadings(PMID);
                                System.out.println("Le nombe de mmmmm  :: "+meshheadings.size());
				for(int j=0;j<meshheadings.size();j++){
					
							bufferedWriter.write("\t"+meshheadings.get(j));
							bufferedWriter.newLine();
						}
				meshheadings.clear();
				bufferedWriter.newLine();
				
			}
			bufferedWriter.close();
			pmids.clear();
			
			
		}
		catch(IOException ex){
			System.err.println("Echec de création du fichier");
		}
 }  
  public static void ExportToRdf(ExportationFiles EF) throws IOException{
          ArrayList<String> pmids;
                 pmids=EF.getListPMIDs();
               
                ArrayList<String> meshheadings;
     
     		File newFile = new File("resultabstract.rdf");
		if(newFile.exists()){
			newFile.delete();
		}
		newFile.createNewFile();
//		
		try{
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile.getAbsolutePath(), true));
			//pmids=EF.getListPMIDs();
			for(int i=0; i<pmids.size();i++){
				String PMID=pmids.get(i);
				//*********** Partie PMID *********************
				bufferedWriter.write("<PubmedArticle id="+PMID+">");
				bufferedWriter.newLine();
				//********* Partie meshHeadings *****************
				bufferedWriter.write("\t\t<MeshheadingsList>");
                                bufferedWriter.newLine();
				meshheadings=EF.getListMeshheadings(PMID);
                                System.out.println("Le nombe de mmmmm  :: "+meshheadings.size());
				for(int j=0;j<meshheadings.size();j++){
					
							bufferedWriter.write("\t\t\t\t"+"<Meshheading>"+meshheadings.get(j)+"</Meshheading>");
							bufferedWriter.newLine();
						}
                                bufferedWriter.write("\t\t</MeshheadingsList>");
                                bufferedWriter.write("</PubmedArticle>");
                                bufferedWriter.newLine();
				meshheadings.clear();
				bufferedWriter.newLine();
				
			}
			bufferedWriter.close();
			pmids.clear();
			
			
		}
		catch(IOException ex){
			System.err.println("Echec de création du fichier");
		}
 }  
 public static void removeAll(DBCollection collection){
	    	collection.remove(new BasicDBObject());
	 }
    

}
