/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdxmlnosql;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Banana
 */
public class ImportattionJSON {
    DBCollection collection;
    String path;
    public ImportattionJSON() throws UnknownHostException{
        collection = new JavaMongoDBConnection().getCollection();
      
        path = new String();
    }
    
    public ImportattionJSON(String path) throws UnknownHostException{
        collection = new JavaMongoDBConnection().getCollection();
        System.out.println("nom de la collection"+collection.getName());
        this.path = path;
    }
    
    public String getPath(){
        return path;
    }  
    
    public void setPath(String path){
        this.path = path;
    }
    
    public DBCollection getCollection(){
        return this.collection;
    }
    
    public void setCollectiion(DBCollection collection){
        this.collection = collection;
    }
    
    public void Import() throws IOException, ParseException{
        BasicDBObject document = new BasicDBObject();
        String jsonText = JsonToString();
        ContainerFactory containerFactory = new ContainerFactory(){
            public List creatArrayContainer() {
                 return new LinkedList();
            }

            public Map createObjectContainer() {
                return new LinkedHashMap();
            }                     
	};
        
         document.putAll((Map)(new JSONParser().parse(jsonText, containerFactory)));
         
      	 collection.insert(document);
    }
    
    public String JsonToString() throws IOException{
        String texte = new String();
	BufferedReader b=new BufferedReader(new FileReader(path));
	String ligne;
	while((ligne=b.readLine())!=null){
            texte+=ligne;
	}
	b.close();
	return texte;
    }
    
}
