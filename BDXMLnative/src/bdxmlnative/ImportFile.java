/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdxmlnative;

import java.io.File;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;

/**
 *
 * @author Banana
 */
public class ImportFile {
    public final String URI = "xmldb:exist://localhost:8080/exist/xmlrpc";
	ConnexionDB connection;
	Collection col;
	String collection, chemin ;
	
	public ImportFile(String chemin) throws XMLDBException, ClassNotFoundException, InstantiationException, IllegalAccessException{
		connection = new ConnexionDB();
		collection="/db/XMLFILES";
		this.chemin =chemin ;
		
		col = DatabaseManager.getCollection(URI + collection);
		System.out.println("bbbbb : "+col);
		if(col == null) {
			Collection root = DatabaseManager.getCollection(URI + "/db");
			CollectionManagementService mgtService = (CollectionManagementService)root.getService("CollectionManagementService", "1.0");
			col = mgtService.createCollection(collection.substring("/db".length()));
		}
		
		// créer une nouvelle XMLResource; un id sera affecté à la nouvelle ressource
		XMLResource document = (XMLResource)col.createResource(chemin.substring(chemin.lastIndexOf('/')+1, chemin.indexOf('.')), "XMLResource");
		
		File f = new File(chemin);
		if(!f.canRead()) {
			System.out.println("Problème de lecture " + chemin);
			return;
		}
		document.setContent(f);
		col.storeResource(document);
		}
	
}
