/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdxmlnative;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;

/**
 *
 * @author Banana
 */
public class ConnexionDB {
    private String driver;
    private Class classe;
    private Database database;
	
    public ConnexionDB() throws ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException{
		driver = "org.exist.xmldb.DatabaseImpl";
		classe = Class.forName(driver);
		Database database = (Database)classe.newInstance();
		DatabaseManager.registerDatabase(database);
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Class getClasse() {
        return classe;
    }

    public void setClasse(Class classe) {
        this.classe = classe;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
        
        
}
		
