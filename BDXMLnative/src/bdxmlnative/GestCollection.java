/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdxmlnative;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XPathQueryService;

/**
 *
 * @author Banana
 */
public class GestCollection {
 LinkedList<String> stopWords;
ConnexionDB connexion;
Collection col;
XPathQueryService service;
LinkedList<String> PMIDs;
LinkedList<String> meshheadings;

//    public GestCollection(String chemin) throws Exception{
//        connexion = new ConnexionDB();
//        this.chemin = chemin;
//        
//        Collection collection = DatabaseManager.getCollection(rootColl+"XMLFILES");
//        XMLResource ressource = (XMLResource)collection.createResource(chemin.substring(chemin.lastIndexOf('/')+1, chemin.indexOf('.')), "XMLResource");
//       
//        File fichier = new File(chemin);
//        ressource.setContent(fichier);
//	collection.storeResource(ressource);
//     
//        
//        
//    }
	
	public GestCollection() throws ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		super();
		connexion = new ConnexionDB();
		col = DatabaseManager.getCollection("xmldb:exist://localhost:8080/exist/xmlrpc/db/XMLFILES" );
		service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
		service.setProperty("indent", "yes");
		String[] tableau = { "A", "ABOUT", "ABOVE", "D" ,"ACCORDING","ACROSS","ACROSS","ACTUAL","ADDED","AFTER"
    ,"AGAINST","AHEAD","ALL","ALMOST","ALONE","ALONG","ALSO","AMONG","AMONGST","AN","AND","AND-OR","ANON","ANOTHER","ANY"
    ,"ARE","ARISING","AROUND","AS","AT","AWARD","AWAY","BE","BECAUSE","BECOME","BECOMES","BEEN","BEFORE","BEHIND","BEING",
    "BELOW","BEST","BETTER","BETWEEN","BEYOND","BIRTHDAY","BOTH","BUT","BY","CAN","CERTAIN","COME","COMES","COMING","COMPLETELY",
    "CONCERNING","CONSIDER","CONSIDERED","CONSIDERING","CONSISTING","DE","DEPARTMENT","DER","DESPITE","DISCUSSION","DO","DOES",
    "DOESNT","DOING","DOWN","DR","DU","DUE","DURING","EACH","EITHER","ESPECIALLY","ET","FEW","FOR","FORWARD","FROM","FURTHER","GET",
    "GIVE","GIVEN","GIVING","HAS","HAVE","HAVING" ,"HIS" ,"HONOR" ,"HOW" ,"IN" ,"INSIDE" ,"INSTEAD" ,"INTO" ,"IS" ,"IT" ,"ITEMS" ,"ITS" ,
    "JUST" ,"LET" ,"LETS" ,"LITTLE" ,"LOOK" ,"LOOKS" ,"MADE" ,"MAKE" ,"MAKES" ,"MAKING" ,"MANY"};
		PMIDs = new LinkedList<String>();
		meshheadings = new LinkedList<String>();
		stopWords = new LinkedList<String>();
                 for(int i=0;i<tableau.length;i++){
                         stopWords.add(tableau[i]);
                 }
	}

	public GestCollection(LinkedList<String> stopWords) throws ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		super();
		connexion = new ConnexionDB();
		col = DatabaseManager.getCollection("xmldb:exist://localhost:8080/exist/xmlrpc/db/XMLFILES/infection.xml" );
		service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
		service.setProperty("indent", "yes");
		this.stopWords = stopWords;
	}

	
	public XPathQueryService getService() {
		return service;
	}

	public void setService(XPathQueryService service) {
		this.service = service;
	}
	
	public LinkedList<String> getPMIDs() {
		return PMIDs;
	}

	public void setPMIDs(LinkedList<String> pMIDs) {
		PMIDs = pMIDs;
	}


	public LinkedList<String> getMeshheadings() {
		return meshheadings;
	}

	public void setMeshheadings(LinkedList<String> meshheadings) {
		this.meshheadings = meshheadings;
	}
        
	public LinkedList<String> withoutStopwords(String texte){
		LinkedList<String> result = new LinkedList<String>();
		String splitText[] = texte.split(" ");
		for(int i=0; i<splitText.length; i++){
			if(!stopWords.contains((String)splitText[i].toUpperCase()))
				result.add(splitText[i]);
		}
		return result;
	}
	
	public void getAllPMID() throws XMLDBException{
		ResourceSet result = service.query("//*[name()='PMID']/text()");
		ResourceIterator i = result.getIterator();
		System.out.println("Taille de PMID : "+ result.getSize());
		while(i.hasMoreResources()) {
			Resource r = i.nextResource();
			PMIDs.add((String)r.getContent());
		}
		System.out.println("Fonction Fini");
	}
	
        //MeshheadingsPMID
	public void getMeshheadingsPMID(String PMID) throws XMLDBException{
		ResourceSet result = service.query("//PubmedArticleSet/PubmedArticle/MedlineCitation[PMID='"
                        +PMID+"']/MeshHeadingList/MeshHeading/DescriptorName/text()");
		ResourceIterator i = result.getIterator();
		while(i.hasMoreResources()) {
			Resource r = i.nextResource();
			meshheadings.add((String)r.getContent());
		}
	}
        //title
	public String getTitre(String PMID) throws XMLDBException{
		ResourceSet result = service.query("//PubmedArticleSet/PubmedArticle/MedlineCitation[PMID='"+PMID+"']/Article/ArticleTitle/text()");
		ResourceIterator i = result.getIterator();
		while(i.hasMoreResources()) {
			Resource r = i.nextResource();
			return (String)r.getContent();
		}
		return new String();
	}
        //export to txt file
	public void TxtWithMeshTerms() throws IOException, XMLDBException{
		File newFile = new File("meshterms.txt");
		if(newFile.exists()){
			newFile.delete();
		}
		newFile.createNewFile();
		
		try{
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile.getAbsolutePath(), true));
			getAllPMID();
			for(int i=0; i<PMIDs.size();i++){
				String PMID=PMIDs.get(i);
				
				bufferedWriter.write("******************************");
				bufferedWriter.newLine();
				bufferedWriter.write("PMID : "+PMID);
				bufferedWriter.newLine();
				bufferedWriter.write("******************************");
				bufferedWriter.newLine();
			
				bufferedWriter.write("Title : \t\t"+getTitre(PMID));;
				bufferedWriter.newLine();
				
				bufferedWriter.write("Meshheading : ");
				getMeshheadingsPMID(PMID);
				for(int j=0;j<meshheadings.size();j++){
					if(j==0)
						bufferedWriter.write("\t"+meshheadings.get(j));
					else{
						bufferedWriter.newLine();
						bufferedWriter.write("\t \t \t \t"+meshheadings.get(j));
					}
				}
				meshheadings.clear();
				bufferedWriter.newLine();
			}
			
			bufferedWriter.close();
			PMIDs.clear();
			
			
		}
		catch(IOException ex){
			System.err.println("Echec de création du fichier");
		}
		
	}
	
        //export to csv file
	public void CSVFileMeshTermsList() throws IOException, XMLDBException{
		File newFile = new File("meshterms.csv");
		if(newFile.exists()){
			newFile.delete();
		}
		newFile.createNewFile();
		
		try{
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile.getAbsolutePath(), true));
			getAllPMID();
			String line = new String();
			line = line + "PMID;Titre;Meshheading";
			bufferedWriter.write(line);
			bufferedWriter.newLine();
			for(int i=0; i<PMIDs.size();i++){
				line = new String();
				String PMID=PMIDs.get(i);
				
				line = PMID+";"+getTitre(PMID)+";";
				
				getMeshheadingsPMID(PMID);
				for(int j=0;j<meshheadings.size();j++){
					if(j==0){
						line= line +meshheadings.get(j);
						bufferedWriter.write(line);
						bufferedWriter.newLine();
					}
					else{
						line = new String();
						line = ";;"+meshheadings.get(j);
						bufferedWriter.write(line);
						bufferedWriter.newLine();
					}
				}
				
				meshheadings.clear();
				bufferedWriter.newLine();
				
			}
			bufferedWriter.close();
			PMIDs.clear();
			
			
		}
		catch(IOException ex){
			System.err.println("Echec de création du fichier");
		}
				
	}
	public LinkedList<String> getStopWords() {
		return stopWords;
	}

	public void setStopWords(LinkedList<String> stopWords) {
		this.stopWords = stopWords;
	}
	
	
	public ConnexionDB getConnexion() {
		return connexion;
	}

	public void setConnexion(ConnexionDB connexion) {
		this.connexion = connexion;
	}

	public Collection getCol() {
		return col;
	}

	public void setCol(Collection col) {
		this.col = col;
	}

            
    } 
      
