/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetbd;

import CRUD.Medelinecitationcrud;
import Models.Abstracttext;
import Models.Author;
import Models.Medelinecitation;
import Models.Meshheading;
import Service.CodeSQL;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Banana
 */
public class Main {
     private static boolean attext;
     private static boolean bName;
     private static boolean bAge;
     private static boolean bRole;
     private static boolean year;
     private static boolean month;
     private static boolean day;
     private static boolean kwb;
     
     //private static boolean pubmedcit;
     
       public static void main(String[] args) throws FileNotFoundException, ParseException {
           //Partie count
        CodeSQL codesql = new CodeSQL();
       System.out.println("nombre de medelinecitation dans la base : "+codesql.countMedliecitation());  
           
       //Partie importation
        String fileName = "C:\\Users\\Banana\\Documents\\NetBeansProjects\\ProjetBDXML\\src\\XML\\infection.xml";
         XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try{
         XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(new FileInputStream(fileName));
    
       Set<Medelinecitation> mc =getMedelinecitation(xmlStreamReader);
       System.out.println(mc.toString());
       System.out.println("nombre de medelinecitation : "+mc.size());
      
        Medelinecitationcrud mcc= new Medelinecitationcrud();
       for(Medelinecitation MC : mc){
           System.out.println("------------------------");
            System.out.println(MC.toString());
           System.out.println("------------------------");
            mcc.addMedlinecitation(MC);
       }
       
      
       List<Medelinecitation> List =mcc.getAllMedlinecitation();
       System.out.println("taille de la liste:"+List.size());
       
       
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
      }

//Partie test
//Medelinecitationcrud mccc = new Medelinecitationcrud();
//Medelinecitation med1 = new Medelinecitation();
//med1.setPmid("1");
//Medelinecitation med=new Medelinecitation();
// med = mccc.getByPMID(med1);
//System.out.println("0000oooooo0000"+med.toString());
    }

       private static Set<Medelinecitation> getMedelinecitation(XMLStreamReader xmlStreamReader) throws XMLStreamException, ParseException{
         Set<Medelinecitation> empList = new HashSet<>();
          Medelinecitationcrud mcc= new Medelinecitationcrud();
         boolean titreBool = false ;
         boolean dateCreation = false;
         boolean dateCompleted=false;
         boolean pmid = false;
         
         Set<Abstracttext> abstractions = new HashSet<Abstracttext>();
         Set<Author> authors = new HashSet<Author>();
         Set<Meshheading> meshheadings = new HashSet<Meshheading>();

         Medelinecitation mc = null;
         String datecompleted="";
         String datecreation="";
         
         
         
         while(xmlStreamReader.hasNext()){
            switch(xmlStreamReader.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("MedlineCitation")){
                        System.out.println("00000000000000000000000"+xmlStreamReader.getAttributeValue(0).toString());
                        mc=new Medelinecitation();
                        mc.setStatus(xmlStreamReader.getAttributeValue(0));
                        System.out.println("Statuuuuuuuuuuuuus"+mc.getStatus().toString());
                        //mc.setStatus(xmlStreamReader.getAttributeValue(0));
                        mc.setOwner(xmlStreamReader.getAttributeValue(1));
                    }else if(xmlStreamReader.getLocalName().equals("PMID")){
                         System.out.println("1111111111111111");
                         mc.setVersionpmid(xmlStreamReader.getAttributeValue(0));
                        pmid=true;
                    }else if(xmlStreamReader.getLocalName().equals("DateCreated")){
                        datecreation=getDate(xmlStreamReader);
                        dateCreation=true;
                    }else if(xmlStreamReader.getLocalName().equals("DateCompleted")){
                        datecompleted=getDate(xmlStreamReader);
                        dateCompleted = true;
                    }else if(xmlStreamReader.getLocalName().equals("ArticleTitle")){
                        titreBool=true;
                    }
                    else if(xmlStreamReader.getLocalName().equals("Abstract")){
                        abstractions = getAbstract(xmlStreamReader);
                        mc.setAbstracttexts(abstractions);
                    }
                    else if(xmlStreamReader.getLocalName().equals("AuthorList")){
                        authors= getAuthor(xmlStreamReader);
                        mc.setAuthors(authors);
                    }
                    else if(xmlStreamReader.getLocalName().equals("MeshHeadingList")){
                        meshheadings= getMeshheadings(xmlStreamReader);
                        mc.setMeshheadings(meshheadings);
                    }
                        
                    break;
                case XMLStreamConstants.CHARACTERS:
                   
                    if(pmid){
                        mc.setPmid(xmlStreamReader.getText());
                        System.out.println("PMID : "+mc.getPmid().toString());
                        pmid=false;
                    }else if(titreBool){
                        mc.setTitre(xmlStreamReader.getText());
                        titreBool=false;
                    }else if(dateCreation){
                        mc.setDatecreation(datecreation);
                        dateCreation=false;
                    }else if(dateCompleted){
                        mc.setDaterevison(datecompleted);
                        dateCompleted=false;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("MedlineCitation")){
                       
                       
//                        mc=mcc.getLast();
//                        for(Iterator it = abstractions.iterator();it.hasNext();){
//                            Abstracttext abs = (Abstracttext)it.next();
//                            mc.getAbstracttexts().add(abs);
//                        }
//                        for(Iterator it = authors.iterator();it.hasNext();){
//                            Author auth = (Author)it.next();
//                            mc.getAuthors().add(auth);
//                        }
//                        
//                        for(Iterator it = meshheadings.iterator();it.hasNext();){
//                            Meshheading mesh = (Meshheading)it.next();
//                            mc.getMeshheadings().add(mesh);
//                        }
                        empList.add(mc);
                        mcc.addMedlinecitation(mc);
                        
                    }  if(xmlStreamReader.getLocalName().equals("PubmedArticleSet")){
                       // mc.setAbstracttexts(abstractions);
                        return empList;
                        
                    }
                    
                    break;
                }
                if (!xmlStreamReader.hasNext())
                    break;
 
             xmlStreamReader.next();
            }
        return empList;
       }
      
    
       private static String getDate(XMLStreamReader xmlStreamReader) throws XMLStreamException, ParseException {
        
        //String y="",d="",m="";
        Datetype date = new Datetype();
        while(xmlStreamReader.hasNext()){
            //System.out.println("in while");
            switch(xmlStreamReader.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                   if(xmlStreamReader.getLocalName().equals("Year")){
                      year=true;
                     // date.setYear(0);
                    }else if(xmlStreamReader.getLocalName().equals("Month")){
                      month=true;    
                    //date.setMonth(0);
                    }else if(xmlStreamReader.getLocalName().equals("Day")){
                       //date.setDay(0);
                       day=true;
                            } 
                    break;
                     case XMLStreamConstants.CHARACTERS:
                   
                    if(year){
                        date.setYear(Integer.parseInt(xmlStreamReader.getText()));
                        year=false;
                    }else if(month){
                        date.setMonth(Integer.parseInt(xmlStreamReader.getText()));
                        month=false;
                    }else if(day){
                        date.setDay(Integer.parseInt(xmlStreamReader.getText()));
                        day=false;
                    }
                    break;
               
                case XMLStreamConstants.END_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("Day"))
                        return ""+date.getDay()+"/"+date.getMonth()+"/"+date.getYear();
                    break;
                }
                if (!xmlStreamReader.hasNext())
                    break;
 
             xmlStreamReader.next();
            }
        return ""+date.getDay()+"/"+date.getMonth()+"/"+date.getYear();
       }

       private static Set<Abstracttext> getAbstract(XMLStreamReader xmlStreamReader) throws XMLStreamException , ParseException {
        Set<Abstracttext> empList = new HashSet<>();
        Abstracttext at = null;
             while(xmlStreamReader.hasNext()){
             switch(xmlStreamReader.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                        if(xmlStreamReader.getLocalName().equals("AbstractText")){
                        at = new Abstracttext();
                        attext=true;
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if(attext){
                        at.setAbstract_(xmlStreamReader.getText());
                        attext=false;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("AbstractText")){
                        empList.add(at);
                    }
                    if(xmlStreamReader.getLocalName().equals("Abstract")){
                        return empList;
                      
                    }
                    break;
                }
                if (!xmlStreamReader.hasNext())
                    break;
                    xmlStreamReader.next();
            }
             
        return empList;
    }
    
       private static Set<Meshheading> getMeshheadings(XMLStreamReader xmlStreamReader) throws XMLStreamException , ParseException {
        Set<Meshheading> empList = new HashSet<>();
        Meshheading kw = null;
             while(xmlStreamReader.hasNext()){
             switch(xmlStreamReader.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                                   if(xmlStreamReader.getLocalName().equals("MeshHeading")){
                        kw = new Meshheading();
                  
                    }if(xmlStreamReader.getLocalName().equals("DescriptorName")){
                           kwb=true;     }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if(kwb){
                        kw.setDescriptorname(xmlStreamReader.getText());
                       kwb=false;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("MeshHeading")){
                        empList.add(kw);
                        
                    }if(xmlStreamReader.getLocalName().equals("MeshHeadingList")){
                        return empList;
                        
                    }
                    
                    break;
                }
                if (!xmlStreamReader.hasNext())
                    break;
                    xmlStreamReader.next();
            }
             
        return empList;
    }
    
       private static Set<Author> getAuthor(XMLStreamReader xmlStreamReader) throws XMLStreamException {
        Set<Author> empList = new HashSet<>();
        Author emp = null;
             while(xmlStreamReader.hasNext()){
              switch(xmlStreamReader.getEventType()) {
              case XMLStreamConstants.START_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("Author")){
                        System.out.println("autheur ouvert");
                        emp = new Author();
                       // emp.setIdAuthor(Integer.parseInt(xmlStreamReader.getAttributeValue(0)));;
                    }else if(xmlStreamReader.getLocalName().equals("LastName")){
                        bName=true;
                    }else if(xmlStreamReader.getLocalName().equals("ForeName")){
                        bAge=true;
                    }else if(xmlStreamReader.getLocalName().equals("Initials")){
                        bRole=true;
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if(bName){
                        emp.setLastname(xmlStreamReader.getText());
                        bName=false;
                    }else if(bAge){
                        emp.setForname(xmlStreamReader.getText());
                        bAge=false;
                    }else if(bRole){
                        emp.setInitials(xmlStreamReader.getText());
                        bRole=false;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if(xmlStreamReader.getLocalName().equals("Author")){
                        empList.add(emp);
                       
                      
                    }
                    if(xmlStreamReader.getLocalName().equals("AuthorList")){
                        
                        return empList;
                      
                    }
                    break;
                }
                if (!xmlStreamReader.hasNext())
                    break;
                xmlStreamReader.next();
            }
        
        return empList;
    }

}
