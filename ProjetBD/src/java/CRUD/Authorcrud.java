/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Models.Author;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import projetbd.NewHibernateUtil;

/**
 *
 * @author Banana
 */
public class Authorcrud {
    public List<Author> getAllAuthors(){
        List<Author> authors = new ArrayList<Author>();
        Transaction trns=null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
         try {
             trns = session.beginTransaction();
             authors = session.createQuery("from Author").list();
         }catch (RuntimeException e){
             e.printStackTrace();
         }finally {
         session.flush();
         session.close();
         }
        return authors;
    }
    public void addauthor(Author author){
        Transaction trns = null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.save(author);
                    session.getTransaction().commit();
                }catch (RuntimeException e){
                    if(trns != null){
                        trns.rollback();
                    }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
}
}
