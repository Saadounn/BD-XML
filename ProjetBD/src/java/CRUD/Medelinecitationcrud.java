/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Models.Medelinecitation;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import projetbd.NewHibernateUtil;

/**
 *
 * @author Banana
 */
public class Medelinecitationcrud {
     public List<Medelinecitation> getAllMedlinecitation(){
        List<Medelinecitation> medlineCitations = new ArrayList<Medelinecitation>();
        Transaction trns=null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
         try {
             trns = session.beginTransaction();
             medlineCitations = session.createQuery("from medelinecitation").list();
         }catch (RuntimeException e){
             e.printStackTrace();
         }finally {
         session.flush();
         session.close();
         }
        return medlineCitations;
    }
    public void addMedlinecitation(Medelinecitation medlinecitation){
        Transaction trns = null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.save(medlinecitation);
                    trns.commit();
                }catch (RuntimeException e){
                    if(trns != null){
                        trns.rollback();
                    }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
    }
    
    public Medelinecitation getByPMID(Medelinecitation medlinecitation){
        List<Medelinecitation> liste = new ArrayList<Medelinecitation>();
        Transaction trns=null;
        Medelinecitation med = null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
         try {
             trns = session.beginTransaction();
             Query query = session.createSQLQuery("from medelinecitation m where m.idmedlinecitation = :pmid ");
             System.out.println("Passé 1 ");
             query.setParameter("pmid", medlinecitation.getPmid());
             //System.out.println("Passé 2 " + medlinecitation.getPmid());
             System.out.println("Passé 2 ");
             liste = query.list();
             System.out.println("Passé 1 ");
             System.out.println("Nombre de medelinecitation trouvé : " +liste.size());
             if(liste.size()>0)
                 med=(Medelinecitation) query.list().get(0);
             trns.commit();
         }catch (RuntimeException e){
         }finally {
         session.flush();
         session.close();
         }
        return med;
    }
}
