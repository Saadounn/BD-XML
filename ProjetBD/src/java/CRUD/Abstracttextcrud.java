/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Models.Abstracttext;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import projetbd.NewHibernateUtil;


/**
 *
 * @author Banana
 */
public class Abstracttextcrud {
      public List<Abstracttext> getAllAbstracttexts(){
        List<Abstracttext> abstracttexts = new ArrayList<Abstracttext>();
        Transaction trns=null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
         try {
             trns = session.beginTransaction();
             abstracttexts = session.createQuery("from Abstracttext").list();
         }catch (RuntimeException e){
             e.printStackTrace();
         }finally {
         session.flush();
         session.close();
         }
        return abstracttexts;
    }
    public void addabstracttext(Abstracttext abstracttext){
        Transaction trns = null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.save(abstracttext);
                    session.getTransaction().commit();
                }catch (RuntimeException e){
                    if(trns != null){
                        trns.rollback();
                    }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
}
}
