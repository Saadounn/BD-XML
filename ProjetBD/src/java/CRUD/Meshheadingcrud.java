/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Models.Meshheading;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import projetbd.NewHibernateUtil;

/**
 *
 * @author Banana
 */
public class Meshheadingcrud {
     public List<Meshheading> getAllpublicationtypes(){
        List<Meshheading> meshheadings = new ArrayList<Meshheading>();
        Transaction trns=null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
         try {
             trns = session.beginTransaction();
             meshheadings = session.createQuery("from Meshheading").list();
         }catch (RuntimeException e){
             e.printStackTrace();
         }finally {
         session.flush();
         session.close();
         }
        return meshheadings;
    }
    public void addpublicationtype(Meshheading publicationtype){
        Transaction trns = null;
        Session	session = NewHibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.save(publicationtype);
                    session.getTransaction().commit();
                }catch (RuntimeException e){
                    if(trns != null){
                        trns.rollback();
                    }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
}
}
