/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import projetbd.NewHibernateUtil;

/**
 *
 * @author Banana
 */
public class CodeSQL {
    private SessionFactory sessionF;
	public SessionFactory getSessionF() {
		return sessionF;
	}
	public void setSessionF(SessionFactory sessionF) {
		this.sessionF = sessionF;
	}
	public CodeSQL(SessionFactory sessionF) {
		super();
		this.sessionF = sessionF;
	}
	public CodeSQL() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void deleteAll(){
		Transaction ts=null;
                Session	session = NewHibernateUtil.getSessionFactory().openSession();
		try
                 {	
			ts = session.beginTransaction();
			//session.delete();
			
			String stringQuery = "DELETE FROM Meshheading";
			Query query = session.createQuery(stringQuery);
			query.executeUpdate();
			
			stringQuery = "DELETE FROM Abstracttext";
			query = session.createQuery(stringQuery);
			query.executeUpdate();
			
			stringQuery = "DELETE FROM Author";
			query = session.createQuery(stringQuery);
			query.executeUpdate();
			
			stringQuery = "DELETE FROM Medelinecitation";
			query = session.createQuery(stringQuery);
			query.executeUpdate();
			
			ts.commit();
        }
		catch (Exception e)
	    {
			if(ts != null)
				ts.rollback();
	        e.printStackTrace();
	    }
	    finally 
	    {
	    	session.close();
	    }
	}
	
	/***************************************************************************************************/
	/*************************Partie Count ************************************************************/
	/***************************************************************************************************/
	public int countMedliecitation(){
		Transaction ts=null;
                Session	session = NewHibernateUtil.getSessionFactory().openSession();
		int count =0;
		try
        {	
			ts = session.beginTransaction();
			count = ((Long)session.createQuery("select count(*) from Medelinecitation").uniqueResult()).intValue();
			ts.commit();
			
        }
		catch (Exception e)
	    {
			if(ts != null)
				ts.rollback();
	        e.printStackTrace();
	    }
	    finally 
	    {
	    	session.close();
	    	return count;
	    }
	}
	
	public int countMeshheading(){
		Transaction ts=null;
                Session	session = NewHibernateUtil.getSessionFactory().openSession();
		int count =0;
		try
        {	
			ts = session.beginTransaction();
			count = ((Long)session.createQuery("select count(*) from Meshheading").uniqueResult()).intValue();
			ts.commit();
			
        }
		catch (Exception e)
	    {
			if(ts != null)
				ts.rollback();
	        e.printStackTrace();
	    }
	    finally 
	    {
	    	session.close();
	    	return count;
	    }
	}
	
	public int countAuthor(){
		Transaction ts=null;
                Session	session = NewHibernateUtil.getSessionFactory().openSession();
		int count =0;
		try
        {	
			ts = session.beginTransaction();
			count = ((Long)session.createQuery("select count(*) from Author").uniqueResult()).intValue();
			ts.commit();
			
        }
		catch (Exception e)
	    {
			if(ts != null)
				ts.rollback();
	        e.printStackTrace();
	    }
	    finally 
	    {
	    	session.close();
	    	return count;
	    }
	}
	
	public int countAbstractiotext(){
		Transaction ts=null;
                Session	session = NewHibernateUtil.getSessionFactory().openSession();
		int count =0;
		try
        {	
			ts = session.beginTransaction();
			count = ((Long)session.createQuery("select count(*) from Abstracttext").uniqueResult()).intValue();
			ts.commit();
			
        }
		catch (Exception e)
	    {
			if(ts != null)
				ts.rollback();
	        e.printStackTrace();
	    }
	    finally 
	    {
	    	session.close();
	    	return count;
	    }
	}
	
	public void countAll(){
		System.out.println("--------------------------------------------------------");
		System.out.println("Count medlinecitation : "+countMedliecitation());
		System.out.println("--------------------------------------------------------");
		System.out.println("--------------------------------------------------------");
		System.out.println("Count meshheading : "+countMeshheading());
		System.out.println("--------------------------------------------------------");
		System.out.println("--------------------------------------------------------");
		System.out.println("Count Author : "+countAuthor());
		System.out.println("--------------------------------------------------------");
		System.out.println("--------------------------------------------------------");
		System.out.println("Count Abstractiontext : "+countAbstractiotext());
		System.out.println("--------------------------------------------------------");
	}

}
